FROM php:7.4-fpm
RUN apt-get update && apt-get install -y curl git zip
ADD . /app
WORKDIR /app
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install
RUN chmod +x /app/flooder.php
CMD php ./flooder.php
